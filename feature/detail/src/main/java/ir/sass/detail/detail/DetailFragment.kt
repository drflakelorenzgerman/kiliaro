package ir.sass.detail.detail
import androidx.navigation.fragment.navArgs
import ir.sass.base_ui.BaseFragmentWithOutViewModel
import ir.sass.data.utils.toReal
import ir.sass.detail.R
import ir.sass.detail.databinding.FragmentDetailBinding
import ir.sass.domain.model.AlbumModelItem

class DetailFragment : BaseFragmentWithOutViewModel<FragmentDetailBinding>(R.layout.fragment_detail) {

    val args by navArgs<DetailFragmentArgs>()

    override fun binding() {
        val realData = toReal<AlbumModelItem>(args.data)
        dataBinding.data = realData
        dataBinding.webView.loadUrl(realData.resizeThumbnail(512,512))
    }

}