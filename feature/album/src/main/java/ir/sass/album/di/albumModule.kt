package ir.sass.album.di

import ir.sass.album.album.AlbumFragmentViewModel
import ir.sass.domain.useCase.album_feature.FetchDataFromApiUseCase
import ir.sass.domain.useCase.album_feature.GetAllAlbumUseCase
import ir.sass.domain.useCase.album_feature.RefreshAndDeleteCacheUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val albumModule = module {
    single {
        GetAllAlbumUseCase(get())
    }
    single {
        RefreshAndDeleteCacheUseCase(get())
    }
    single {
        FetchDataFromApiUseCase(get())
    }
    viewModel<AlbumFragmentViewModel>{
        AlbumFragmentViewModel(get(),get(),get())
    }
}