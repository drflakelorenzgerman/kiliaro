package ir.sass.album.album

import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import bg.dihanov.navigation.NavigationFlow
import ir.sass.album.R
import ir.sass.album.databinding.FragmentAlbumBinding
import ir.sass.album.databinding.ItemAlbumImageBinding
import ir.sass.base_ui.BaseAdapter
import ir.sass.base_ui.BaseFragment
import ir.sass.base_ui.RecyclerItemWrapper
import ir.sass.data.utils.toJsonString
import ir.sass.domain.model.AlbumModelItem
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlbumFragment : BaseFragment<FragmentAlbumBinding,AlbumFragmentViewModel>(R.layout.fragment_album) {

    override val viewModel: AlbumFragmentViewModel by viewModel()

    val adapter = BaseAdapter<ItemAlbumImageBinding,AlbumModelItem>(
        RecyclerItemWrapper(R.layout.item_album_image,
            mutableListOf()){binding,item,_ ->
            binding.item = item
            binding.root.setOnClickListener {
                navigate(NavigationFlow.Detail(toJsonString(item)))
            }
        }
    ).apply {
        setHasStableIds(true)
    }


    override fun binding() {
        dataBinding.adapter = adapter
        dataBinding.lm = GridLayoutManager(requireContext(),3)

        lifecycleScope.launchWhenCreated {
            launch {
                viewModel.albumFlow.collect {
                    it?.let {
                        adapter.replaceList(it.toMutableList())
                    }
                }
            }
        }

        dataBinding.swiperefresh.setOnRefreshListener {
            dataBinding.swiperefresh.isRefreshing = false
            viewModel.refresh()
        }
    }

    override fun bindingOnce() {
        super.bindingOnce()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getAlbum()
        }
    }
}