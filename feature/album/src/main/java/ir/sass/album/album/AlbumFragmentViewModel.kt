package ir.sass.album.album

import androidx.lifecycle.viewModelScope
import ir.sass.base_ui.BaseViewModel
import ir.sass.domain.model.AlbumModelItem
import ir.sass.domain.useCase.album_feature.FetchDataFromApiUseCase
import ir.sass.domain.useCase.album_feature.GetAllAlbumUseCase
import ir.sass.domain.useCase.album_feature.RefreshAndDeleteCacheUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class AlbumFragmentViewModel(
    val getAllAlbumUseCase: GetAllAlbumUseCase,
    val refreshAndDeleteCacheUseCase : RefreshAndDeleteCacheUseCase,
    val fetchDataFromApiUseCase: FetchDataFromApiUseCase
) : BaseViewModel() {
    private val _albumFlow : MutableStateFlow<List<AlbumModelItem>?> = MutableStateFlow(null)
    val albumFlow : StateFlow<List<AlbumModelItem>?> = _albumFlow

    private val sharedKey = "djlCbGusTJamg_ca4axEVw"

    fun getAlbum(){
        viewModelScope.launch {
            getAllAlbumUseCase.run(sharedKey).collect {
                _albumFlow.emit(it)
            }

            launch {
                changeLoading(true)
                fetchDataFromApiUseCase.run(sharedKey).collect {
                    handleError(it,true){
                        changeLoading(false)
                    }
                }
            }
        }
    }

    fun refresh(){
        viewModelScope.launch {
            changeLoading(true)
            refreshAndDeleteCacheUseCase.run(sharedKey).collect {
                handleError(it,true){
                    changeLoading(false)
                }
            }
        }
    }
}