package ir.sass.kiliaro.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import bg.dihanov.navigation.NavigationFlow
import bg.dihanov.navigation.Navigator
import bg.dihanov.navigation.ToFlowNavigatable
import ir.sass.kiliaro.R
import ir.sass.kiliaro.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()  ,
    ToFlowNavigatable {

    lateinit var dataBinding : ActivityMainBinding
    lateinit var navController: NavController
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityMainBinding.inflate(
            LayoutInflater.from(this)
        )
        setContentView(dataBinding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        navigator = Navigator().apply {
            navController = this@MainActivity.navController
        }
    }

    override fun navigateToFlow(flow: NavigationFlow) {
        navigator.navigateToFlow(flow)
    }
}