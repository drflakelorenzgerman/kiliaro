package ir.sass.kiliaro.app

import android.app.Application
import ir.sass.album.di.albumModule
import ir.sass.data.di.apiModule
import ir.sass.data.di.dbModule
import ir.sass.data.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            val modules = listOf(repositoryModule, apiModule,dbModule,albumModule)
            modules(modules)
        }
    }
}