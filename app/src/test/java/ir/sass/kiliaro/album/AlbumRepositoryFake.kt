package ir.sass.kiliaro.album

import com.nhaarman.mockitokotlin2.mock
import ir.sass.domain.model.AlbumModelItem
import ir.sass.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class AlbumRepositoryFake(val success : Boolean) : AlbumRepository {

    private val data = mutableListOf<AlbumModelItem>()

    override suspend fun getAllAlbum(sharedKey: String): Flow<List<AlbumModelItem>> = flow {
        emit(data)
    }

    override suspend fun fetchAlbumsFromApi(sharedKey: String): Flow<Result<Boolean>> = flow {
        data.add(mock())
        emit(if(success) Result.success(true) else Result.failure<Boolean>(Throwable("AlbumRepositoryFake")))
    }

    override suspend fun refresh(sharedKey: String): Flow<Result<Boolean>> = flow {
        data.clear()
        data.add(mock())
        emit(if(success) Result.success(true) else Result.failure<Boolean>(Throwable("AlbumRepositoryFake")))
    }

}