package ir.sass.kiliaro.album

import app.cash.turbine.test
import ir.sass.album.album.AlbumFragmentViewModel
import ir.sass.data.base.BaseTest
import ir.sass.domain.repository.AlbumRepository
import ir.sass.domain.useCase.album_feature.FetchDataFromApiUseCase
import ir.sass.domain.useCase.album_feature.GetAllAlbumUseCase
import ir.sass.domain.useCase.album_feature.RefreshAndDeleteCacheUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

class AlbumFragmentViewModelTest : BaseTest() {

    lateinit var fakeRepositoryFake: AlbumRepositoryFake
    val fakeSharedKey = "fakeSharedKey"

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `check if the data will be streamed over the GetAllAlbumUseCase when we called getAll()`() = testScope.runTest {
        val viewModel = getSuccessViewModel()
        viewModel.getAlbum()
        viewModel.albumFlow.test {
            delay(100) // this library need some time
            assert(this.expectMostRecentItem()!!.size == 1)
        }
    }


    @ExperimentalCoroutinesApi
    @Test
    fun `check if the data will be failed in a correct way when we called getAll()`() = testScope.runTest {
        val viewModel = getFailedViewModel()
        viewModel.getAlbum()
        viewModel.error.test {
            delay(100) // this library need some time
            assert(this.expectMostRecentItem().message.equals("AlbumRepositoryFake"))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `check if we refresh then we get new data or not`() = testScope.runTest {
        val viewModel = getSuccessViewModel()
        viewModel.refresh()
        fakeRepositoryFake.getAllAlbum(fakeSharedKey).test {
            delay(100) // this library need some time
            assert(this.expectMostRecentItem().size == 1)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `check if we refresh and we get error doesn't Result is Failed or not`() = testScope.runTest {
        val viewModel = getFailedViewModel()
        viewModel.refresh()
        fakeRepositoryFake.refresh(fakeSharedKey).test {
            delay(100) // this library need some time
            assert(this.expectMostRecentItem().isFailure)
        }
    }

    private fun getSuccessViewModel() : AlbumFragmentViewModel{
        fakeRepositoryFake = AlbumRepositoryFake(true)
        return AlbumFragmentViewModel(
            GetAllAlbumUseCase(fakeRepositoryFake), RefreshAndDeleteCacheUseCase(fakeRepositoryFake),
            FetchDataFromApiUseCase((fakeRepositoryFake))
        )
    }

    fun getFailedViewModel() : AlbumFragmentViewModel{
        fakeRepositoryFake = AlbumRepositoryFake(false)
        return AlbumFragmentViewModel(
            GetAllAlbumUseCase(fakeRepositoryFake), RefreshAndDeleteCacheUseCase(fakeRepositoryFake),
            FetchDataFromApiUseCase((fakeRepositoryFake))
        )
    }
}