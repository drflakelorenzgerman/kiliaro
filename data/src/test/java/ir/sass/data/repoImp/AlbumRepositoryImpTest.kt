package ir.sass.data.repoImp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import ir.sass.data.base.BaseDataTest
import ir.sass.data.dataSource.local.AlbumDao
import ir.sass.data.dataSource.local.AlbumDataBase
import ir.sass.data.dataSource.local.entities.AlbumModelItemDb
import ir.sass.data.dataSource.online.ApiService
import ir.sass.data.repositoryImp.AlbumRepositoryImp
import ir.sass.domain.model.AlbumModelItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class AlbumRepositoryImpTest : BaseDataTest() {
    lateinit var daoFake : AlbumDao
    lateinit var apiService : ApiService
    lateinit var mockModel : List<AlbumModelItem>
    lateinit var modelStub : AlbumModelItem
    lateinit var modelDbStub : AlbumModelItemDb
    private val fakeSharedKey = "fakeSharedKey"
    private lateinit var repo : AlbumRepositoryImp


    @Before
    fun setUp() {
        apiService = mock()
        mockModel = mock()
        modelDbStub = AlbumModelItemDb(
            "1","1","1","1","1","1","1",
            1,1,1,"1","1"
        )
        modelStub = AlbumModelItem(
            "1","1","1","1","1","1","1",
            1,1,1,"1","1"
        )

        daoFake = object : AlbumDao{

            private val data : MutableList<AlbumModelItemDb> = mutableListOf(modelDbStub)

            override fun getAll(): Flow<List<AlbumModelItemDb>> = flow { emit(data) }

            override fun insertAll(items: List<AlbumModelItemDb>) {data.addAll(items)}

            override fun delete() {data.clear()}
        }
        repo = AlbumRepositoryImp(apiService,daoFake)
    }


    @ExperimentalCoroutinesApi
    @Test
    fun `check if we fetch data from Api then APiService will be called`() = testScope.runTest {
        whenever(apiService.getAllAlbum(fakeSharedKey)).thenReturn(mockModel)
        repo.fetchAlbumsFromApi(fakeSharedKey)
        verify(apiService, times(1)).getAllAlbum(fakeSharedKey)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `check if we want to get all Album list from Local then dao will emit some values`() = testScope.runTest {
        repo.getAllAlbum(fakeSharedKey).collect {
            assert(it.size == 1)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `check if we refreshed the data then tha apiService will fectch data again`() = testScope.runTest {
        repo.refresh(fakeSharedKey)
        whenever(apiService.getAllAlbum(fakeSharedKey)).thenReturn(listOf(modelStub))
        verify(apiService, times(1)).getAllAlbum(fakeSharedKey)
    }
}