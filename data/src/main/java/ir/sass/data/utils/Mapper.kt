package ir.sass.data.utils

import com.google.gson.Gson


// all the mapper interface/function should be added here


interface  Mapper<T> {
    fun toDomain() : T
}



fun <T>toJsonString(input : T) : String = Gson().toJson(input).toString()

inline fun <reified T>toReal(input : String) : T = Gson().fromJson(input,T::class.java)