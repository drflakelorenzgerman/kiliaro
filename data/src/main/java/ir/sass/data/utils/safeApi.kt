package ir.sass.data.utils

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

// this method can be extended for further feature
// we can add different type of Eror for example for net or etc

suspend fun <T> safeApi( req : suspend () -> T  ) : Result<T> = withContext(IO){
    try {
        return@withContext Result.success<T>(req.invoke())
    }catch (e : Throwable){
        return@withContext Result.failure<T>(e)
    }
}
