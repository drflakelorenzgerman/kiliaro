package ir.sass.data.dataSource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.sass.data.dataSource.local.entities.AlbumModelItemDb

const val ALBUM_TABLE_NAME = "album"

@Database(entities = [AlbumModelItemDb::class],version = 1)
abstract class AlbumDataBase : RoomDatabase() {
    abstract fun albumDao() : AlbumDao
}