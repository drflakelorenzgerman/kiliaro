package ir.sass.data.dataSource.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ir.sass.data.dataSource.local.ALBUM_TABLE_NAME
import ir.sass.data.utils.Mapper
import ir.sass.domain.model.AlbumModelItem

@Entity(tableName = ALBUM_TABLE_NAME)
class AlbumModelItemDb(
    @ColumnInfo(name = "content_type") val content_type: String,
    @ColumnInfo(name = "created_at") val created_at: String,
    @ColumnInfo(name = "download_url") val download_url: String,
    @ColumnInfo(name = "filename") val filename: String,
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "md5sum") val md5sum: String,
    @ColumnInfo(name = "media_type") val media_type: String,
    @ColumnInfo(name = "resx") val resx: Int,
    @ColumnInfo(name = "resy") val resy: Int,
    @ColumnInfo(name = "size") val size: Int,
    @ColumnInfo(name = "thumbnail_url") val thumbnail_url: String,
    @ColumnInfo(name = "user_id") val user_id: String
) : Mapper<AlbumModelItem>{
    override fun toDomain(): AlbumModelItem = AlbumModelItem(
        content_type,
        created_at,
        download_url,
        filename,
        id,
        md5sum,
        media_type,
        resx,
        resy,
        size,
        thumbnail_url,
        user_id
    )

    companion object{
        fun toDB(item: AlbumModelItem) = AlbumModelItemDb(
            item.content_type,
            item.created_at,
            item.download_url,
            item.filename,
            item.id,
            item.md5sum,
            item.media_type,
            item.resx,
            item.resy,
            item.size,
            item.thumbnail_url,
            item.user_id
        )
    }
}