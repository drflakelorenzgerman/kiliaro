package ir.sass.data.dataSource.local

import androidx.room.Dao
import androidx.room.Query
import ir.sass.data.dataSource.local.entities.AlbumModelItemDb
import kotlinx.coroutines.flow.Flow
import androidx.room.FtsOptions.Order
import androidx.room.Insert

import androidx.room.OnConflictStrategy




@Dao
interface AlbumDao {
    @Query("SELECT * FROM $ALBUM_TABLE_NAME")
    fun getAll(): Flow<List<AlbumModelItemDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<AlbumModelItemDb>)

    @Query("DELETE FROM $ALBUM_TABLE_NAME")
    fun delete()
}