package ir.sass.data.dataSource.online

import ir.sass.domain.model.AlbumModelItem
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("/shared/{sharedkey}/media")
    suspend fun getAllAlbum(@Path("sharedkey")sharedkey : String) : List<AlbumModelItem>
}