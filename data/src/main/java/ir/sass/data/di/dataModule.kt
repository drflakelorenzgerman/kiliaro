package ir.sass.data.di

import androidx.room.Room
import ir.sass.data.dataSource.local.AlbumDataBase
import ir.sass.data.dataSource.online.ApiService
import ir.sass.data.repositoryImp.AlbumRepositoryImp
import ir.sass.domain.repository.AlbumRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val repositoryModule = module {
    single<AlbumRepository> {
        AlbumRepositoryImp(get(),get())
    }
}

val apiModule = module {
    single<ApiService> {
        Retrofit.Builder()
            .baseUrl("https://api1.kiliaro.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}

val dbModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            AlbumDataBase::class.java,
            "album_database")
            .build().albumDao()
    }
}