package ir.sass.data.repositoryImp

import ir.sass.data.dataSource.local.AlbumDao
import ir.sass.data.dataSource.local.entities.AlbumModelItemDb
import ir.sass.data.dataSource.online.ApiService
import ir.sass.data.utils.safeApi
import ir.sass.domain.model.AlbumModelItem
import ir.sass.domain.repository.AlbumRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class AlbumRepositoryImp(
    private val apiService: ApiService,
    val albumDao: AlbumDao
) : AlbumRepository {


    // getting data from local db for offline first usage
    override suspend fun getAllAlbum(sharedKey: String): Flow<List<AlbumModelItem>> {
           return albumDao.getAll().map {
               it.map {
                   it.toDomain()
               }
            }.flowOn(IO)
        }

    // every time we fetch data from Api we'll replace it on dataBase too
    override suspend fun fetchAlbumsFromApi(sharedKey: String) : Flow<Result<Boolean>> {
        val remoteRet = safeApi { apiService.getAllAlbum(sharedKey) }
        if(remoteRet.isSuccess){
            CoroutineScope(IO).launch {
                albumDao.insertAll(remoteRet.getOrThrow().map {
                    AlbumModelItemDb.toDB(it)
                })
            }
            return flow {
                emit(Result.success(true))
            }
        }else{
            return flow {
                emit(Result.failure(Throwable(remoteRet.exceptionOrNull())))
            }
        }
    }

    // refresh will clear data from local and fetch data from Api
    override suspend fun refresh(sharedKey: String): Flow<Result<Boolean>>{
        CoroutineScope(IO).launch {
            albumDao.delete()
        }
        return fetchAlbumsFromApi(sharedKey)
    }
}