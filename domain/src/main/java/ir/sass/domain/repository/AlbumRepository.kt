package ir.sass.domain.repository

import ir.sass.domain.model.AlbumModelItem
import kotlinx.coroutines.flow.Flow

interface AlbumRepository {
    suspend fun getAllAlbum(sharedKey: String) : Flow<List<AlbumModelItem>>

    suspend fun fetchAlbumsFromApi(sharedKey : String): Flow<Result<Boolean>>

    suspend fun refresh(sharedKey : String): Flow<Result<Boolean>>
}