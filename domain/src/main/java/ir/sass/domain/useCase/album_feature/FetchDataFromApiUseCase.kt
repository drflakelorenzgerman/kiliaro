package ir.sass.domain.useCase.album_feature

import ir.sass.domain.repository.AlbumRepository
import ir.sass.domain.useCase.BaseUseCase
import kotlinx.coroutines.flow.Flow

class FetchDataFromApiUseCase(
    private val albumRepository: AlbumRepository
) : BaseUseCase<String, Flow<Result<Boolean>>> {

    override suspend fun run(input: String): Flow<Result<Boolean>> = albumRepository.fetchAlbumsFromApi(input)

}