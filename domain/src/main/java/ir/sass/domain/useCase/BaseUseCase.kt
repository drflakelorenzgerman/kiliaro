package ir.sass.domain.useCase

interface BaseUseCase<in Input, out Output> {
    suspend fun run(input: Input) : Output
}

interface BaseUseCaseWithOutInput<out Output> {
    suspend fun run() : Output
}

interface BaseUseCaseWithOutOutput<in Input>{
    suspend fun run(input: Input)
}
