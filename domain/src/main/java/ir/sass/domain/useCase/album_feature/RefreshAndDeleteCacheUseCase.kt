package ir.sass.domain.useCase.album_feature

import ir.sass.domain.repository.AlbumRepository
import ir.sass.domain.useCase.BaseUseCase
import kotlinx.coroutines.flow.Flow

class RefreshAndDeleteCacheUseCase(
    private val albumRepository: AlbumRepository
) : BaseUseCase<String, Flow<Result<Boolean>>> {

    override suspend fun run(input: String) = albumRepository.refresh(input)
}