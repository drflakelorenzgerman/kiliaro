package ir.sass.domain.useCase.album_feature

import ir.sass.domain.model.AlbumModelItem
import ir.sass.domain.repository.AlbumRepository
import ir.sass.domain.useCase.BaseUseCase
import ir.sass.domain.useCase.BaseUseCaseWithOutInput
import kotlinx.coroutines.flow.Flow

class GetAllAlbumUseCase(
    private val albumRepository: AlbumRepository
) : BaseUseCase<String,Flow<List<AlbumModelItem>>> {

    override suspend fun run(input: String): Flow<List<AlbumModelItem>> = albumRepository.getAllAlbum(input)

}