package ir.sass.domain.model

data class AlbumModelItem(
    val content_type: String,
    val created_at: String,
    val download_url: String,
    val filename: String,
    val id: String,
    val md5sum: String,
    val media_type: String,
    val resx: Int,
    val resy: Int,
    val size: Int,
    val thumbnail_url: String,
    val user_id: String
){
    fun resizeThumbnail(w : Int,h :Int ,m : String) = thumbnail_url+"?w=${w}&h=${h}&m=${m}"

    fun resizeThumbnail(w : Int,h :Int) = thumbnail_url+"?w=${w}&h=${h}"


    fun sizeInFloat() = size.toFloat()

    companion object{
        const val CROP = "crop"
        const val BoundingBox = "bb"
        const val MinimumDimensions = "md"
    }
}