package ir.sass.base_ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import ir.sass.base_ui.databinding.LottieImageViewBinding

class LottieImageView
@JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): FrameLayout(context,attrs,defStyleAttr){
    val binding : LottieImageViewBinding
    init {
        binding = LottieImageViewBinding.inflate(
            LayoutInflater.from(context),this,true
        )
    }
}

@BindingAdapter("app:url")
fun setImageUrl(img: LottieImageView, url: String?) {

    url?.let {
        img.binding.lottie.playAnimation()

        Glide.with(img.context)
            .load(url)
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .addListener(imageLoadingListener(img.binding.lottie))
            .into(img.binding.img)
    }

}

fun imageLoadingListener(pendingImage: LottieAnimationView): RequestListener<Drawable?> {
    return object : RequestListener<Drawable?> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable?>?, isFirstResource: Boolean): Boolean {
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: com.bumptech.glide.request.target.Target<Drawable?>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            pendingImage.pauseAnimation()
            pendingImage.visibility = View.GONE
            return false
        }
    }
}