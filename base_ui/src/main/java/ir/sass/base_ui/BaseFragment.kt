package ir.sass.base_ui

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class BaseFragment<T : ViewDataBinding,ViewModel : BaseViewModel>(
    layout : Int
) : BaseFragmentWithOutViewModel<T>(layout) /*, CoroutineScope */{

    abstract protected val viewModel : BaseViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            launch {
                viewModel.error.collect{
                    // we can later extend this part, for example navigating to a bottom sheet or sth like that
                    requireContext().toast(it.message?:"Error")
                }
            }
            launch {
                // the reason I used Int over Boolean for this part is maybe we'll have couple of
                // request at a same time and the true/false system won't work for that situation
                viewModel.loading.collect {
                    if(it > 0){
                        dataBindingOuter.progressCircular.visibility = View.VISIBLE
                    }else{
                        dataBindingOuter.progressCircular.visibility = View.GONE
                    }
                }
            }
        }

        if(!viewModel.bindingOnce){
            viewModel.bindingOnce = true
            bindingOnce()
        }

    }

    // sometimes we need one time binding
    open fun bindingOnce(){}


}
