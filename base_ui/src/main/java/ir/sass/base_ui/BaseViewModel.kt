package ir.sass.base_ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {
    private val _loading : MutableStateFlow<Int> = MutableStateFlow(0)
    val loading : StateFlow<Int> = _loading

    private val _error : MutableSharedFlow<Throwable> = MutableSharedFlow()
    val error : SharedFlow<Throwable> = _error

    var bindingOnce = false

    protected fun changeLoading(active : Boolean){
        _loading.value += if(active) 1 else -1
        if(_loading.value < 0) _loading.value = 0
    }

    private fun changeError(exception: Throwable){
        viewModelScope.launch {
            _error.emit(exception)
        }
    }

    fun <T>handleError(result: Result<T>,decreaseLoading : Boolean,action : (T)->Unit){
        if(result.isSuccess){
            action.invoke(result.getOrThrow())
        }else{
            changeError(result.exceptionOrNull()?:Throwable("Unknown"))
            if(decreaseLoading)
                changeLoading(false)
        }
    }
}