package ir.sass.base_ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import bg.dihanov.navigation.NavigationFlow
import bg.dihanov.navigation.ToFlowNavigatable
import ir.sass.base_ui.databinding.FragmentBaseBinding

abstract class BaseFragmentWithOutViewModel<T : ViewDataBinding>(
    val layout : Int
) : Fragment(R.layout.fragment_base){

    protected lateinit var dataBinding : T
    internal lateinit var dataBindingOuter : FragmentBaseBinding
    lateinit var flowNavigator : ToFlowNavigatable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBindingOuter =
            FragmentBaseBinding.inflate(inflater,container,false)
        dataBinding = DataBindingUtil.inflate(layoutInflater,layout,dataBindingOuter.viewAdder,true)
        flowNavigator = requireActivity() as ToFlowNavigatable

        return dataBindingOuter.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding()
    }

    abstract fun binding()


    fun navigate(flow: NavigationFlow){
        flowNavigator.navigateToFlow(flow)
    }

}
