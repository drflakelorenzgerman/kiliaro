package ir.sass.base_ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

// every RecyclerView should use this Adapter
// if you want to have more option you can extend from it

class BaseAdapter<DB : ViewDataBinding , DataType> (
    private val wrapper: RecyclerItemWrapper<DB,DataType>
        ) : RecyclerView.Adapter<BaseAdapter.BaseAdapterVH<DB>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAdapterVH<DB> =
        BaseAdapterVH(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), wrapper.layout, parent, false)
        )

    override fun onBindViewHolder(
        holder: BaseAdapterVH<DB>,
        position: Int
    ) {
        wrapper.bindingFun.invoke(holder.binding,wrapper.list[position],position)
    }

    override fun getItemCount(): Int = wrapper.list.size

    class BaseAdapterVH<DB : ViewDataBinding>(var binding : DB) : RecyclerView.ViewHolder(binding.root)

    fun addToList(list: List<DataType>){
        val lastPos = wrapper.list.size
        wrapper.list.addAll(list)
        notifyItemRangeChanged(lastPos,wrapper.list.size)
    }

    fun replaceList(list: List<DataType>){
        val lastSize = wrapper.list.size
        wrapper.list.clear()
        notifyItemRangeRemoved(0,lastSize)
        addToList(list)
    }

    override fun getItemId(position: Int): Long = position.toLong()

}

// this wrapper include layout and list, it also include a lambda for binding


data class RecyclerItemWrapper<DB : ViewDataBinding , DataType>(
    @LayoutRes
    var layout : Int,var list : MutableList<DataType>,
    val bindingFun : (binding : DB,item : DataType,pos : Int) -> Unit
)
