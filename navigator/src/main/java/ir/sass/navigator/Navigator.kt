package bg.dihanov.navigation

import android.os.Bundle
import androidx.navigation.NavController
import ir.sass.navigator.R

// navigator can navigate between module
// for navigation inside a feature module we'll use the default navigation system
// for navigation between module we have to add that module in NavigationFlow sealed class
// maybe that approach is not right because it violate the SOLID (O part) but
// I've search a lot about this issue before and the best practice I've found was this approach
// we can use deepLink too but I didn't like it, we can delete the sealed class and
// replace it with a interface/extend system too

class Navigator {
    lateinit var navController: NavController

    fun navigateToFlow(navigationFlow: NavigationFlow) = when (navigationFlow) {
            is NavigationFlow.Detail->{
                navController.navigate(R.id.global_to_detail,Bundle().apply { putString("data",navigationFlow.data) })
            }
            else->{
                // Nothing for now
            }
    }

    fun resetNavigation(){
        navController.setGraph(R.navigation.app_navigation)
    }
}

