package bg.dihanov.navigation

sealed class NavigationFlow {
    class Detail(val data : String) : NavigationFlow()
}