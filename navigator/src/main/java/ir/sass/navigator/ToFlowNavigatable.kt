package bg.dihanov.navigation

// we can have other options too
// for example in another project I've use another method
// for hiding navigation Button and etc but for now this is enough

interface ToFlowNavigatable {
    fun navigateToFlow(flow: NavigationFlow)
}